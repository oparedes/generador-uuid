const fs=require('fs');
const https=require('https');
require('dotenv').config();

const express = require('express');
const app = express();
const {WebhookClient} = require('dialogflow-fulfillment');
//const mysql = require('mysql');
let consultasql = require('./consultasql');

const httpsServerOptions = {
    key: fs.readFileSync(process.env.KEY_PATH),
    cert: fs.readFileSync(process.env.CERT_PATH),
};


const serverHttps=https.createServer(httpsServerOptions,app);
serverHttps.listen(process.env.HTTPS_PORT,process.env.IP);
 
app.get('/', function (req, res) {
  res.send('Función de prueba')
})

app.post('/webhook', express.json(),function (req, res) {
    const agent = new WebhookClient({ request:req, response:res });
    console.log('Dialogflow Request headers: ' + JSON.stringify(req.headers));
    console.log('Dialogflow Request body: ' + JSON.stringify(req.body));
   
    function welcome(agent) {
      agent.add(`Welcome to my agent!`);
    }

    function fallback(agent) {
      //agent.add(`I didn't understand`);
      agent.add(`Pruebas de maternidad1`);

    }    
   
/*     function pruebas(agent) {
      //agent.add(`I didn't understand`);
      agent.add(`Pruebas de maternidad`);
      console.log(agent.parameters.numerocop);
    } */

    function ProbandoWebhook(agent) {
        for (let i = 1; i < 6; i++) {
            agent.add(`Probando `+i);
            
        }
        
        agent.add(`Probando webhook`);
      }    




  




      function InicioSI(agent) {
        return consultasql.queryInicioSI(consultasql.conexionsqlserver(),agent)
        .then(result=>{
          //console.log("inicio");
          //console.log(result);
          //console.log(result[0]);
          //console.log(result[0][0].color);
          if ( Object.keys(result[0]).length === 0 ) {
            agent.add("Lo siento, el numero de colegiatura digitado no se encuentra en el sistema, corregir");

          } else {
              agent.add("Muchas gracias "+result[0][0].nom_pat+" "+result[0][0].nom_mat+" "+result[0][0].Nombres+"  por la información brindada.  Indique el área con la que desea comunicarse: Decanato, Mesa de partes, Administración,   Economía,               Logística, Planificación, FPS,  Sistemas,  Asesoría Legal o Colegiatura.");
          }
        });        
      }            

//
function EstadoSolicitudBeneficioBeter(agent) {
  const tipobeneficio="201";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioAyuda(agent) {
  const tipobeneficio="202";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioInvalidez(agent) {
  const tipobeneficio="102";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioBefac(agent) {
  const tipobeneficio="200";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioFer(agent) {
  const tipobeneficio="108";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioFallecimiento(agent) {
  const tipobeneficio="103";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioEnfermedad(agent) {
  const tipobeneficio="101";
  return querySQL(agent,tipobeneficio);        
}      
function EstadoSolicitudBeneficioMaternidad(agent) {
  const tipobeneficio="107";
  return querySQL(agent,tipobeneficio);        
}      

function querySQL(agent,tipobeneficio) {
return  consultasql.querySolicitudEstadoBeneficio(consultasql.conexionsqlserver(),agent,tipobeneficio)
  .then(result=>{
    //console.log(result);
    console.log("FUNCION");
    console.log(result[0]);
    //console.log(result[0][0].estado_solicitud);
    //console.log(result[0][0].color);
    if ( Object.keys(result[0]).length === 0 ) {
      agent.add("No se ha encontrado información en el tipo de beneficio solicitado");

    } else {
      if (result[0][0].estado==='P') {
        agent.add("El estado de su solicitud está PENDIENTE con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip);
      }
      if (result[0][0].estado==='X') {
        agent.add("El estado de su solicitud está PENDIENTE con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip);
      }      
      if (result[0][0].estado==='V') {
        agent.add("El estado de su solicitud está en estado DEVUELTO con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip);
      }            
      if (result[0][0].estado==='A') {
        agent.add("El estado de su solicitud está APROBADO con fecha de solicitud  : "+result[0][0].Fe_Solicitud1+" Fecha de pago: "+result[0][0].Fe_Pago1+" "+result[0][0].Tx_Descrip+" Importe: "+result[0][0].Importe);
      }      
      if (result[0][0].estado==='D') {
        agent.add("El estado de su solicitud está DENEGADO con fecha de solicitud : "+result[0][0].Fe_Solicitud1+" "+result[0][0].Tx_Descrip);
      }            

    }
  });  
}



    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);
    intentMap.set('ProbandoWebhook', ProbandoWebhook);
    //intentMap.set('getDataFromMySQL',handleReadFromMySQL);
    intentMap.set('Maternidadwebhook',EstadoSolicitudBeneficioMaternidad);//
    intentMap.set('Enfermedadwebhook',EstadoSolicitudBeneficioEnfermedad);//
    intentMap.set('Fallecimientowebhook',EstadoSolicitudBeneficioFallecimiento);//
    intentMap.set('Ferwebhook',EstadoSolicitudBeneficioFer);//
    intentMap.set('Befacwebhook',EstadoSolicitudBeneficioBefac);//
    intentMap.set('Invalidezwebhook',EstadoSolicitudBeneficioInvalidez);//
    intentMap.set('Ayudasolidariawebhook',EstadoSolicitudBeneficioAyuda);//
    intentMap.set('Beterwebhook',EstadoSolicitudBeneficioBeter);//
    intentMap.set('BienvenidaSI',InicioSI);//EstadoSolicitudBeneficio
    agent.handleRequest(intentMap);            
    //res.send('Hello World')
  })
 
app.listen(80,()=>{console.log("Servidor Iniciado");})






